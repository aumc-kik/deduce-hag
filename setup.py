
# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
# from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Version from file
version = {}
with open("deduce_hag/version.py") as fp:
    exec(fp.read(), version)
version = version['__version__']

setup(
    name='deduce_hag',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html

    version=version,

    description="Fork of Deduce: de-identification method for Dutch medical text; now for GPs",

    # The project's main homepage.
    url='https://bitbucket.org/aumc-kik/deduce-hag/',

    # Original author details
    #author='Vincent Menger',
    # author_email='v.j.menger@uu.nl',
    # Author details
    author='Torec Luik',
    author_email='t.t.luik@amsterdamumc.nl',

    packages=['deduce_hag'],

    # Data files
    package_data={'deduce_hag': ['data/*']},

    # Choose your license
    license='GNU GPLv3',

    # What does your project relate to?
    keywords='de-identification',

    install_requires=['nltk', 'pandas', 'pytest', 'tqdm'],
)
