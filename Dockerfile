# Use an official Python runtime as a parent image
FROM python:3.7.6

# Set user
USER root

# Install python requirements
RUN pip install --upgrade pip setuptools

# Add requirements separately, but before copying DIR to use cache more
# ADD requirements.txt /requirements.txt

# Install any needed packages specified in requirements.txt
# RUN pip install -r /requirements.txt --timeout 1000

# Copy the parent directory contents into the container at /<app>
# Note: Ignore sensitive files/folders via the .dockerignore file
COPY . /deduce

# Set the working directory to /<app>
WORKDIR /deduce

RUN export LC_ALL=en_US.UTF-8

# Install this python module / source code
RUN pip install .