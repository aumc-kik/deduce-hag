# DEDUCE_HAG: DEDUCE for GPs
This is an adjusted version (a "fork") of DEDUCE for and by Amsterdam UMC, AMC, Primary Care & Clinical Informatics.

Contact Torec Luik (t.t.luik@amsterdamumc.nl) or Jonathan Bouman (j.bouman@amsterdamumc.nl) for questions about changes made w.r.t. DEDUCE. 

Contact original authors for questions about original DEDUCE.

# Changelog
## Version 1.2.3
- Add PullRequest#2, improving DEDUCE performance by using sets instead of lists
## Version 1.2.2
- toevoegen lijsten onbekend geen zorg lab meer onderscheid in instellingen
## Version 1.2.1
- Added Docker support
## Version 1.2.0
- Split Instellingen:
    - Ambulante parazorg + thuiszorg
    - Verblijfsinstellingen
    - Ziekenhuizen
    - GGZ
    - Huisartsenpraktijk
    - Overige instellingen (ungrouped, dagbesteding, eerstelijnsdiagnostiek)
- Added logic for ignoring PERSON annotations while looking for institution names
- Change date logic to exclude the extra characters at the end from the capture group
- Add some items to lists based on Utrecht review
- Update Latin1 tests, remove failing line from csv
## Version 1.1.8
- Hotfix: escape parsed characters in a regex
## Version 1.1.7
- Add generic names, which are only counted as names when Uppercase and in the middle of a sentence
- Filter other name lists with generic names, to deduplicate
- Adjust Date regex to account for start/end of lines
- Add "r" prefix to most regex strings (do not interpret backslash)
- Adjust PostalCode regex to account for end of line and group both options in 1 regex
- Adjust Email regex to trigger more easily on half-emails (blabla@ and @blabla), mostly because of interaction of earlier DEDUCE options (e.g. INSTITUTIONS)
- Adjust longnumber regex to allow a lot more medically-related number patterns via whitelist patterns.
- Add/remove some terms to/from other black/white lists
## Version 1.1.6
- Add terms to blacklists that were found during validation
- Lowercase residences (woonplaats)
- Add name logic to handle 'MvNaam' cases
- Add lowercasing also to names with interfixes
## Version 1.1.5
-	Start splitting INSTITUTIONS into specific
    - hospitals: ziekenhuizen/ ZBC / vrijgevestigde specialisten
    - residential: verpleeghuis/ revalidatiekliniek / hospices
    - outpatient care: fysiotherapie/ logopedie / thuiszorg / anders
-   Turn off INSTITUTIONS by default in scripts
-	Lowercase names, add exceptions to the medical terms list
-	Turn off AGE by default in scripts
-	Turn off Pat. Number by default in scripts, replaced with longnumber
-	< and > replaced with << en >>, and turned back to < and > when done with deduce
-   added medical terms

## Version 1.1.4
- Updated README
- Move version to version.py and output version in read scripts
- Added TQDM progressbar if input_nrows (total rows of input file) is given
- Align output of stream and chunk scripts
- Extend test CSV to cover new cases
- Add annotate_numbers and annotate_numberwords
- Extend white- and blacklists with VUmc specific values
- Add BitBucket testing pipeline and store test results in XML
- Add LGPL to new changes as well
- Reduce names deduce logic to preserve more medical terms
## Version 1.1.3
- Change DEDUCE name detection logic
- Extend whitelist for GP domain
- Update tests
- Add option for to dropping original text column in csv script
## Version 1.1.2
- Add encoding option for latin1 and test script
## Version 1.1.1
- Rename project
- Add scripts to stream csv lines through DEDUCE
- Add pytest and add test
## Version 1.1.0
- Regex changed ...
## Version 1.0.1
- Basic DEDUCE from github (https://github.com/vmenger/deduce.git)

-----

# Deduce: de-identification method for Dutch medical text

This project contains the code for DEDUCE: de-identification method for Dutch medical text as described in [Menger et al (2017)](http://www.sciencedirect.com/science/article/pii/S0736585316307365). De-identification of medical text is needed for using text data for analysis, to comply with legal requirements and to protect the privacy of patients. Our pattern matching based method removes Protected Health Information (PHI) in the following categories:

1. Person names, including initials
2. Geographical locations smaller than a country
3. Names of institutions that are related to patient treatment
4. Dates
5. Ages
6. Patient numbers
7. Telephone numbers
8. E-mail addresses and URLs

The details of the development and workings of the method, and its validation can be found in: 

[Menger, V.J., Scheepers, F., van Wijk, L.M., Spruit, M. (2017). DEDUCE: A pattern matching method for automatic de-identification of Dutch medical text, Telematics and Informatics, 2017, ISSN 0736-5853](http://www.sciencedirect.com/science/article/pii/S0736585316307365)

### Demo

An interactive demo of the code can be found [here](http://ads.science.uu.nl/deduce/)

### Prerequisites

* `nltk`
* `pandas`
* `pytest` (since DEDUCE_HAG)
* `tqdm` (since DEDUCE_HAG)

### Installing

Installing DEDUCE_HAG or the original from source, simply download and use python to install (from the downloaded directory):

``` python
>>> python -m pip install .
```

Installing OF THE ORIGINAL DEDUCE can be done through pip and git: 

``` python
>>> pip install git+https://github.com/vmenger/deduce.git
```

### Docker

Instead of installing the package locally, you can use the provided Dockerfile to build a Docker image for DEDUCE_HAG.
This image will contain the correct Python installation and libraries to run, so that you only need to setup Docker on your system.

```
docker build --tag deduce_hag:<version>
```
Then you can run a script via Docker:
```
docker run deduce_hag:<version> python stream_csv_with_deduce.py
```

To run it on your own data, you'll need to mount your data to the Docker container as you run it  (with the -v option) AND tell the script where to find it.

For example

```
docker run -v /c/users/me/my-data-dir/:/data deduce_hag:1.2.0 python stream_csv_with_deduce.py --input_file /data/my-data-file.csv
```
Note that Windows might have some issues with mounting of folders, depending on your Windows and Docker versions.

We also try to provide a tar of an image of every DEDUCE_HAG version via the pipeline.
## Getting started

The package has a method for annotating (`annotate_text`) and for removing the annotations (`deidentify_annotations`).

``` python

import deduce 

deduce.annotate_text(
        text,                       # The text to be annotated
        patient_first_names="",     # First names (separated by whitespace)
        patient_initials="",        # Initial
        patient_surname="",         # Surname(s)
        patient_given_name="",      # Given name
        names=True,                 # Person names, including initials
        locations=True,             # Geographical locations
        institutions=True,          # Institutions
        dates=True,                 # Dates
        ages=True,                  # Ages
        patient_numbers=True,       # Patient numbers
        phone_numbers=True,         # Phone numbers
        urls=True,                  # Urls and e-mail addresses
        flatten=True                # Debug option
    )    
    
deduce.deidentify_annotations(
        text                        # The annotated text that should be de-identified
    )
    
```

## Examples
``` python
>>> import deduce

>>> text = u"Dit is stukje tekst met daarin de naam Jan Jansen. De patient J. Jansen (e: j.jnsen@email.com, t: 06-12345678) is 64 jaar oud 
    en woonachtig in Utrecht. Hij werd op 10 oktober door arts Peter de Visser ontslagen van de kliniek van het UMCU."
>>> annotated = deduce.annotate_text(text, patient_first_names="Jan", patient_surname="Jansen")
>>> deidentified = deduce.deidentify_annotations(annotated)

>>> print (annotated)
"Dit is stukje tekst met daarin de naam <PATIENT Jan Jansen>. De <PATIENT patient J. Jansen> (e: <URL j.jnsen@email.com>, t: <TELEFOONNUMMER 06-12345678>) 
is <LEEFTIJD 64> jaar oud en woonachtig in <LOCATIE Utrecht>. Hij werd op <DATUM 10 oktober> door arts <PERSOON Peter de Visser> ontslagen van de kliniek van het <INSTELLING umcu>."
>>> print (deidentified)
"Dit is stukje tekst met daarin de naam <PATIENT>. De <PATIENT> (e: <URL-1>, t: <TELEFOONNUMMER-1>) is <LEEFTIJD-1> jaar oud en woonachtig in <LOCATIE-1>.
Hij werd op <DATUM-1> door arts <PERSOON-1> ontslagen van de kliniek van het <INSTELLING-1>."
```
## Scripts (SINCE DEDUCE_HAG)

In the root of the folder you can find 2 scripts to apply DEDUCE_HAG on a (possibly big) csv file: 
1. Streaming the file via [stream_csv_with_deduce.py](stream_csv_with_deduce.py)
    - This script will use the least memory, as it will stream one row at a time
2. Reading the file in chunks via [chunk_csv_with_deduce.py](chunk_csv_with_deduce.py)
    - This will allow you to use more memory and read/write to files less
    - You can control the size of the chunks to read via the script parameter, e.g. ```--chunksize 10000```

For usage details, see:
``` bash
>>> python chunk_csv_with_deduce.py --help

>>> python stream_csv_with_deduce.py --help
```

Note: if you provide no input file, it will run on the provided [test file](anonimization\ set.csv).
Note: if you provide ```--input_nrows``` with the total amount of rows in your input file, the scripts can show a nice progress bar.

### Example
``` bash
>>> python stream_csv_with_deduce.py --input_nrows=121
Version: 1.1.4
Parameters: {'input_file': 'anonimization set.csv', 'input_delimiter': ';', 'input_encoding': 'latin1', 'text_column_name': 'Unanonimized', 'output_file': 'anonimization results v3.csv', 'output_delimiter': ';', 'deduce_column_name': 'DEDUCE HAG', 'input_nrows': 121, 'output_drop_text_column': False, 'output_write_mode': 'w', 'verbose': 0, 'print_interval': 1000, 'deduce_names': True, 'deduce_locations': True, 'deduce_institutions': True, 'deduce_dates': True, 'deduce_ages': True, 'deduce_patient_numbers': True, 'deduce_phone_numbers': True, 'deduce_urls': True, 'deduce_number_words': True, 'deduce_long_numbers': True}
Creating outputfile
100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████| 121/121 [00:00<00:00, 310.26rows/s]

Finished writing
```


### Configuring

The lookup lists in the `data/` folder can be tailored to the users specific needs. This is especially recommended for the list of names of institutions, since they are by default tailored to location of development and testing of the method. Regular expressions can be modified in `annotate.py`, this is for the same reason recommended for detecting patient numbers. 

## Testing (SINCE DEDUCE_HAG)

We use the `pytest` package for testing DEDUCE_HAG. Please run the following to test your installation:
```
>>> pytest
```

## Contributing

Contributions to the deduce project are very much welcomed - feel free to get in touch with the authors via issue or e-mail. 

## Versioning (ORIGINAL DEDUCE)

1.1.1 - Added several csv scripts

1.1.0 - Regex changes

1.0.1 - Small bugfix for None as input

1.0.0 - Initial version

## Authors

* **Vincent Menger** - *Initial work* 
* **Jonathan de Bruin** - *Code review*
* **Torec Luik** - *Added code for DEDUCE_HAG*
* **Jonathan Bouman** - *Regex changes for Primary Care*
* **Frederique van Nouhuys** - *Code and list updates*
* **Jacob Rousseau** - *Code performance updates*

## License

This project is licensed under the GNU LGPLv3 license - see the [LICENSE.md](LICENSE.md) file for details
