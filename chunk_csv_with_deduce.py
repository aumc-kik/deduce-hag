# Copyright (C) 2020 Torec Luik
# 
# DEDUCE_HAG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DEDUCE_HAG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with DEDUCE_HAG. If not, see <http://www.gnu.org/licenses/>.
#
## Read and write csv, while applying Deduce on the text
## Now with argument parsing
## Author: t.t.luik@amsterdamumc.nl

############################### IMPORTS ############################
import csv
import deduce_hag as deduce
import sys
import argparse
import pandas as pd
import numpy as np
from multiprocessing import Pool, cpu_count
from tqdm import tqdm
import math
from deduce_hag import version

############################### SETTINGS ############################
verbose = 0                       # 1 for more logging messages
print_interval = 1             # print update after processing x chunks
n_cores = cpu_count()                        # parallel processing
# wrt input file
input_file = "anonimization set.csv"
input_delimiter = ";"
input_encoding = "latin1"         # List of encodings: https://docs.python.org/3/library/codecs.html#standard-encodings
input_engine = "python"           # Pandas parser engine: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html
text_column_name = 'Unanonimized'
input_chunk = 7500
input_nrows = None                  # Number of rows for progress bar
# wrt output file
output_file = "anonimization results v3.csv"
output_delimiter = ";"
output_drop_text_column = False
deduce_column_name = "DEDUCE HAG"
output_write_mode = 'w'           # 'w' for write; 'a' for append
# wrt deduce
deduce_names = True                 # Person names, including initials
deduce_locations = True             # Geographical locations
deduce_institutions = True         # Institutions
deduce_dates = True                 # Dates
deduce_ages = False                 # Ages
deduce_patient_numbers = False      # Patient numbers
deduce_phone_numbers = True         # Phone numbers
deduce_urls = True                  # Urls and e-mail addresses
deduce_flatten = True               # Debug option
deduce_number_words = True          # Trigger words with numbers, like BSN
deduce_long_numbers = True          # Numbers > 4
glob_args = None

############################### FUNCTIONS ############################


def parallelize_dataframe(df, func, n_cores=n_cores):
    df_split = np.array_split(df, n_cores)
    try:
        pool = Pool(n_cores)
        df = pd.concat(pool.starmap(func, [(split, glob_args) for split in df_split]))
    finally:
        pool.close()
        pool.join()
    return df


def apply_deduce_parallel(df, args):
    return df.apply(lambda txt: apply_deduce(txt, args))


def apply_deduce(text, args):
    annotated = deduce.annotate_text(
        # The text to be annotated
        str(text),
        # First names (separated by whitespace)
        patient_first_names="",
        patient_initials="",                            # Initial
        patient_surname="",                             # Surname(s)
        patient_given_name="",                          # Given name
        # Person names, including initials
        names=args.deduce_names,
        locations=args.deduce_locations,                # Geographical locations
        institutions=args.deduce_institutions,          # Institutions
        dates=args.deduce_dates,                        # Dates
        ages=args.deduce_ages,                          # Ages
        patient_numbers=args.deduce_patient_numbers,    # Patient numbers
        phone_numbers=args.deduce_phone_numbers,        # Phone numbers
        urls=args.deduce_urls,                          # Urls and e-mail addresses
        number_words=args.deduce_number_words,          # Trigger words with numbers
        long_numbers=args.deduce_long_numbers,          # Numbers longer than 4
        flatten=args.deduce_flatten                     # Debug option
    )

    deidentified = deduce.deidentify_annotations(
        annotated        # The annotated text that should be de-identified
    )

    if verbose > 0:
        print("Annotated: \n\n",
              annotated,
              "\n\nDeidentified:\n\n",
              deidentified)
                                 


    return deidentified


def process_csv(args):
    with open(args.output_file, args.output_write_mode, newline='',encoding=args.input_encoding) as output:
        sys.stdout.write('Applying Deduce on inputfile...')

        global glob_args
        glob_args = args

        # Get column names
        cols = pd.read_csv(args.input_file, sep=args.input_delimiter,
                encoding=args.input_encoding,
                nrows=1).columns.tolist()
        # Append the output column name
        cols.append(args.deduce_column_name)
        
        # Output text column 
        if args.output_drop_text_column == True: cols.remove(args.text_column_name)
        
        # progress bar if nrows provided
        if args.input_nrows is not None:
            it = enumerate(tqdm(pd.read_csv(
                args.input_file,
                sep=args.input_delimiter,
                chunksize=args.chunksize,
                encoding=args.input_encoding,
                engine=args.input_engine),
                total=math.ceil(args.input_nrows / args.chunksize),
                unit='chunk'))
        else:
            it = enumerate(pd.read_csv(
                args.input_file,
                sep=args.input_delimiter,
                chunksize=args.chunksize,
                encoding=args.input_encoding,
                engine=args.input_engine))

        for filled_len, chunk in it:
            chunk[args.deduce_column_name] = parallelize_dataframe(
                chunk[args.text_column_name], apply_deduce_parallel)
            if filled_len == 0 and args.output_write_mode == 'w':
                chunk.to_csv(output, sep=args.output_delimiter,
                             header=True, mode='w', index=False, columns=cols)
            else:
                chunk.to_csv(output, sep=args.output_delimiter,
                             header=False, mode='a', index=False, columns=cols)
            # print progress
            if args.input_nrows is None:
                if filled_len % args.print_interval == 0:
                    sys.stdout.write('.')
                    sys.stdout.flush()
        sys.stdout.write("\nFinished writing\n")

############################### SCRIPT ############################


def main():
    # parse arguments from commandline to overwrite default settings
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input_file",
        default=input_file,
        type=str,
        help="The input data file (a csv file)."
    )
    parser.add_argument(
        "--input_delimiter",
        default=input_delimiter,
        type=str,
        help="The input data file (a csv file) delimiter. Default ';'"
    )
    parser.add_argument(
        "--input_engine",
        default=input_engine,
        type=str,
        help="The engine (python/c) to use for reading csv with Pandas. Default 'python'"
    )
    parser.add_argument(
        "--input_encoding",
        default=input_encoding,
        type=str,
        help="The encoding of the input (csv) file. Default 'latin1'"
    )
    parser.add_argument(
        "--input_nrows",
        default=input_nrows,
        type=int,
        help="The number of rows in the input file, for a progress bar"
    )
    parser.add_argument(
        "--chunksize",
        default=input_chunk,
        type=int,
        help="The number of lines to read at one time."
    )
    parser.add_argument(
        "--text_column_name",
        default=text_column_name,
        type=str,
        help="The input data file (a csv file) column name with text."
    )
    parser.add_argument(
        "--output_file",
        default=output_file,
        type=str,
        help="The output csv where the deduced text will be written.",
    )
    parser.add_argument(
        "--output_delimiter",
        default=output_delimiter,
        type=str,
        help="The output csv delimiter.",
    )
    parser.add_argument(
        "--deduce_column_name",
        default=deduce_column_name,
        type=str,
        help="The output csv column name for deduced text.",
    )
    parser.add_argument(
        "--output_write_mode",
        default=output_write_mode,
        type=str,
        help="The output csv write mode. Change to 'a' to append to existing file.",
    )
    parser.add_argument(
        "--output_drop_text_column",
        dest="output_drop_text_column",
        action='store_true',
        help="Drop the original text column in the output; default false (keep the column)"
    )
    parser.add_argument(
        "--no-output_drop_text_column",
        dest="output_drop_text_column",
        action='store_false',
        help="Do not drop the original text column in the output; default true (keep the column)"
    )
    parser.add_argument(
        "--verbose",
        default=verbose,
        type=int,
        help="1 for more logging messages; 0 for least logging",
    )
    parser.add_argument(
        "--print_interval",
        default=print_interval,
        type=int,
        help="Print update after processing this amount of chunks",
    )
    parser.add_argument('--no-deduce_names',
                        dest='deduce_names',
                        action='store_false',
                        help="Don't Deduce Person names, including initials")
    parser.add_argument('--no-deduce_locations',
                        dest='deduce_locations',
                        action='store_false',
                        help="Don't Deduce geographical locations")
    parser.add_argument('--no-deduce_institutions',
                        dest='deduce_institutions',
                        action='store_false',
                        help="Don't Deduce institutions")
    parser.add_argument('--no-deduce_dates',
                        dest='deduce_dates',
                        action='store_false',
                        help="Don't Deduce dates")
    parser.add_argument('--no-deduce_ages',
                        dest='deduce_ages',
                        action='store_false',
                        help="Don't Deduce ages")
    parser.add_argument('--no-deduce_patient_numbers',
                        dest='deduce_patient_numbers',
                        action='store_false',
                        help="Don't Deduce patient numbers")
    parser.add_argument('--no-deduce_phone_numbers',
                        dest='deduce_phone_numbers',
                        action='store_false',
                        help="Don't Deduce phone numbers")
    parser.add_argument('--no-deduce_urls',
                        dest='deduce_urls',
                        action='store_false',
                        help="Don't Deduce urls and e-mail addresses")
    parser.add_argument('--no-deduce_flatten',
                        dest='deduce_flatten',
                        action='store_false',
                        help="Don't flatten tags in DEDUCE (debug option)")
    parser.add_argument('--no-deduce_number_words',
                        dest='deduce_number_words',
                        action='store_false',
                        help="Don't Deduce numberwords, like BSN")
    parser.add_argument('--no-deduce_long_numbers',
                        dest='deduce_long_numbers',
                        action='store_false',
                        help="Don't long (>4) numbers")
    parser.set_defaults(deduce_names=deduce_names,
                        deduce_locations=deduce_locations,
                        deduce_institutions=deduce_institutions,
                        deduce_dates=deduce_dates,
                        deduce_ages=deduce_ages,
                        deduce_patient_numbers=deduce_patient_numbers,
                        deduce_phone_numbers=deduce_phone_numbers,
                        deduce_urls=deduce_urls,
                        deduce_flatten=deduce_flatten,
                        output_drop_text_column=output_drop_text_column,
                        deduce_number_words=deduce_number_words,
                        deduce_long_numbers=deduce_long_numbers)
    args = parser.parse_args()

    print("Version:", version.__version__)

    print("Parameters: {}".format(vars(args)))

    process_csv(args)


if __name__ == "__main__":
    main()
