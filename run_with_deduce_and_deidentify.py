# Copyright (C) 2020 Torec Luik
# 
# DEDUCE_HAG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DEDUCE_HAG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with DEDUCE_HAG. If not, see <http://www.gnu.org/licenses/>.
#
## Simple test script in order to verify the annotations
## Now includes deidentify library (which could include deduce again)
## NOTE: loads dataset fully into memory
## NOTE: requires pandas and deidentify library
## Author: t.t.luik@amsterdamumc.nl
import deduce_hag as deduce_hag
import deduce as deduce_vm
import csv
from deidentify.base import Document
from deidentify.taggers import FlairTagger
from deidentify.tokenizer import TokenizerFactory
from deidentify.util import mask_annotations
import pandas as pd

def deduce_it(testText, deduce, verbose=1):
    annotated = deduce.annotate_text(
        testText,                       # The text to be annotated
        patient_first_names="",     # First names (separated by whitespace)
        patient_initials="",        # Initial
        patient_surname="",         # Surname(s)
        patient_given_name="",      # Given name
        names=True,                 # Person names, including initials
        locations=True,             # Geographical locations
        institutions=True,          # Institutions
        dates=True,                 # Dates
        ages=False,                 # Allow ages
        patient_numbers=True,       # Patient numbers
        phone_numbers=True,         # Phone numbers
        urls=True,                  # Urls and e-mail addresses
        number_words=True,          # Trigger words with numbers
        long_numbers=True,          # Numbers longer than 4
        flatten=True                # Debug option
    )    
        
    deidentified = deduce.deidentify_annotations(
            annotated                        # The annotated text that should be de-identified
        )

    if verbose > 0: print("Annotated: \n\n", annotated, "\n\nDeidentified:\n\n", deidentified)
    
    return annotated, deidentified

def deidentify_it(rows, verbose=1):
    # Wrap text in document
    documents = [ Document(name=f"doc{i}", text=txt) for i, txt in enumerate(rows)]
    
    # Select downloaded model
    model = 'model_bilstmcrf_ons_fast-v0.1.0'

    # Instantiate tokenizer
    tokenizer = TokenizerFactory().tokenizer(corpus='ons', disable=("tagger", "ner"))

    # Load tagger with a downloaded model file and tokenizer
    tagger = FlairTagger(model=model, tokenizer=tokenizer, verbose=False)

    # Annotate your documents
    annotated_docs = tagger.annotate(documents)

    masked_docs = [ mask_annotations(annotated_doc) for annotated_doc in annotated_docs ]

    if verbose > 0: print("Annotated: \n\n", annotated_docs[0], "\n\nDeidentified:\n\n", masked_docs[0])

    return annotated_docs, masked_docs

def print_output(row, de):
    if row['DEDUCE (off-the-shelf)'] == de:
        corr = "(Correct)" if row['Correct?'] == 1 else "(Incorrect)"
        print(f"{de} {corr}")
    else:
        print(f"{de}  <------ (Different!)")

verbose = 0
df = pd.DataFrame(columns=["Unanonimized", "DEDUCE", "DEDUCE HAG", "DEIDENTIFY"])
# read test csv
with open('anonimization set.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';')
    print('Full text:\nDEDUCE_hag: ', mr_hag, '\nDEDUCE_VM: ', mr_vm)
    rows = [row for row in reader]
    an_, de_ = deidentify_it([row['Unanonimized'] for row in rows], verbose=verbose)
    for i, row in enumerate(rows):
        print(f"===== {i}: {row['Unanonimized']}")
        print(">> DEDUCE HAG")
        an, de = deduce_it(row['Unanonimized'], deduce_hag, verbose=verbose)
        print_output(row, de)
        print(">> DEIDENTIFY")
        print_output(row, de_[i].text)
        print(">> DEDUCE (VM)")
        _, de_d = deduce_it(row['Unanonimized'], deduce_vm, verbose=0)
        print_output(row, de_d)

        ## add to df
        df.loc[i] = [row['Unanonimized'], de_d, de, de_[i].text]
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        print(df)
    df.to_csv('anonimization results.csv')



