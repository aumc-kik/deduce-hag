""" This module contains all list reading functionality """
import re
import itertools

from .listtrie import ListTrie
from .utility import read_list, filter_institutions
from .tokenizer import tokenize_split

# Replacement terms
NUMBERWORD = "NUMMERWOORD"
LONGNUMBER = "LANGNUMMER"
PERSON = "PERSOON"
LOCATION = "LOCATIE"
INSTITUTION = "INSTELLING"
HOSPITAL = "ZIEKENHUIS"
RESIDENCE = "VERBLIJFSINSTELLING"
AMBULANTCARE = "AMBULANTEZORG"
MENTALCARE = "GGZ"
PRIMARYCARE = "HUISARTSENPRAKTIJK"
LAB = "LABORATORIUM" 
ONBEKEND = "ONBEKEND"
OVERIG = "OVERIG"  
GEENZ = "GEENZORG"  
DATE = "DATUM"
AGE = "LEEFTIJD"
PATIENTNUMBER = "PATIENTNUMMER"
PHONENUMBER = "TELEFOONNUMMER"
URL = "URL"

#  Read first names
FIRST_NAMES = read_list("voornaam.lst", min_len=2, lower=True)

# TLK add generic names to check more restrictively
# Read generic names (that overlap with meaningful words)
GENERIC_NAMES = read_list("generieke_namen.lst", lower=False)
GENERIC_NAMES_lower = {x.lower()
                       for x in GENERIC_NAMES}  # only for differences

# Read last names
# TLK filter generic names if needed
SURNAMES = read_list("achternaam.lst", encoding="utf-8",
                     min_len=2, normalize=True, lower=True)
SURNAMES = set(SURNAMES).difference(GENERIC_NAMES_lower)

# Read interfixes (such as 'van der', etc)
INTERFIXES = read_list("voorvoegsel.lst")

# Read all surnames that frequently occur with an
# interfix ('Jong', 'Vries' for 'de Jong', 'de Vries', etc)
# TLK filter generic names if needed
INTERFIX_SURNAMES = {line.strip().split(" ")[-1].lower()
                              for line in read_list("achternaammetvv.lst")}
INTERFIX_SURNAMES = set(
    set(INTERFIX_SURNAMES).difference(GENERIC_NAMES_lower))

# Read prefixes (such as mw, dhr, pt)
PREFIXES = read_list("prefix.lst")

# Read a list of medical terms
MEDTERM = read_list("medischeterm.lst", encoding="latin-1")

# Read the top 1000 of most used words in Dutch, and then filter all surnames from it
# TLK filter also other surname lists then
TOP1000 = read_list("top1000.lst", encoding="latin-1")
TOP1000 = {x for x in TOP1000 if x.lower() not in (
    list(SURNAMES) + list(GENERIC_NAMES_lower) + list(INTERFIX_SURNAMES))}

# A list of trigger words for following numbers
NUMBERTRIGGERWORDS = read_list("numbertriggerwords.lst")

# A list of stop words
STOPWORDS = read_list("stopwoord.lst")

# The whitelist of words that are never annotated as names consists of
# the medical terms, the top1000 words and the stopwords
WHITELIST = {line.lower()
                      for line in list(MEDTERM) + list(TOP1000) + list(STOPWORDS) if len(line) >= 2}

# Institutions

# Read the list
INSTITUTIONS = read_list("instellingen.lst", min_len=3)

# TLK Add sublists into one detection list
INSTITUTIONS_GGZ = read_list("instellingen_ggz.lst", min_len=3)
INSTITUTIONS_HAP = read_list("instellingen_huisartsenpraktijken.lst", min_len=3)
INSTITUTIONS_ZKH = read_list("instellingen_ziekenhuizen.lst", min_len=3)
INSTITUTIONS_VBL = read_list(
    
    "instellingen_verblijfsinstellingen.lst", min_len=3)
INSTITUTIONS_APZ = read_list("instellingen_ambulanteparazorg.lst", min_len=3)
INSTITUTIONS_LAB = read_list("lab.lst", min_len=3)
INSTITUTIONS_ONB = read_list("onbekend.lst", min_len=3)
INSTITUTIONS_OVR = read_list("overig.lst", min_len=3)
INSTITUTIONS_GNZ = read_list("geenzorg.lst", min_len=3)

# New list of institutions
FILTERED_INSTITUTIONS = {}

# Iterate over all institutions
INSTITUTIONS = filter_institutions(INSTITUTIONS)
INSTITUTIONS_APZ = filter_institutions(INSTITUTIONS_APZ)
INSTITUTIONS_GGZ = filter_institutions(INSTITUTIONS_GGZ)
INSTITUTIONS_HAP = filter_institutions(INSTITUTIONS_HAP)
INSTITUTIONS_VBL = filter_institutions(INSTITUTIONS_VBL)
INSTITUTIONS_ZKH = filter_institutions(INSTITUTIONS_ZKH)
INSTITUTIONS_LAB = filter_institutions(INSTITUTIONS_LAB)
INSTITUTIONS_ONB = filter_institutions(INSTITUTIONS_ONB)
INSTITUTIONS_OVR = filter_institutions(INSTITUTIONS_OVR)
INSTITUTIONS_GNZ = filter_institutions(INSTITUTIONS_GNZ)
# Concat all institutions
FILTERED_INSTITUTIONS = list(itertools.chain(INSTITUTIONS, INSTITUTIONS_APZ,
                                           
                                             INSTITUTIONS_GGZ,  INSTITUTIONS_HAP,  INSTITUTIONS_VBL,  INSTITUTIONS_ZKH,  INSTITUTIONS_LAB,  INSTITUTIONS_ONB,  INSTITUTIONS_OVR,  INSTITUTIONS_GNZ ))

# Remove doubles, occurrences on whitelist, and convert back to list
INSTITUTIONS = list(set(FILTERED_INSTITUTIONS).difference(WHITELIST))
# Remember mapping of institutions



def specify_institution_type(term):
    '''
    Further specify the type of institution if possible
    '''
    if term in INSTITUTIONS_ZKH:
        return HOSPITAL
    elif term in INSTITUTIONS_VBL:
        return RESIDENCE
    elif term in INSTITUTIONS_APZ:
        return AMBULANTCARE
    elif term in INSTITUTIONS_GGZ:
        return MENTALCARE
    elif term in INSTITUTIONS_HAP:
        return PRIMARYCARE
    elif term in INSTITUTIONS_LAB:
        return LAB
    elif term in INSTITUTIONS_ONB:
        return ONBEKEND
    elif term in INSTITUTIONS_OVR:
        return OVERIG
    elif term in INSTITUTIONS_GNZ:
        return GEENZ
    else:
        return INSTITUTION



INSTITUTIONS_MAP = {x:  specify_institution_type(x) for x in INSTITUTIONS}

# Residences

# Read the list
RESIDENCES = read_list("woonplaats.lst", encoding="utf-8", normalize=True)

# Remove parentheses from the names
RESIDENCES = {re.sub("\(.+\)", "", residence) for residence in RESIDENCES}

# Strip values and remove doubles again
RESIDENCES_SET = {residence.strip() for residence in RESIDENCES}

# New copy
FILTERED_RESIDENCES = set(RESIDENCES_SET)

# Also add the version with hyphen (-) replaced by whitespace
for residence in RESIDENCES_SET:
    if "-" in residence:
        FILTERED_RESIDENCES.add(re.sub("\-", " ", residence))

# Reinitialize set of RESIDENCES
RESIDENCES_SET = set()

# Remove all RESIDENCES that are on the whitelist
for residence in FILTERED_RESIDENCES:
    if residence.lower() not in WHITELIST:
        RESIDENCES_SET.add(residence)

# TLK lowercase residences
RESIDENCES = {x.lower() for x in RESIDENCES_SET}

# Define some tries, to make lookup faster

INSTITUTION_TRIE = ListTrie()
RESIDENCES_TRIE = ListTrie()

for institution in INSTITUTIONS:
    INSTITUTION_TRIE.add(tokenize_split(institution))

for residence in RESIDENCES:
    RESIDENCES_TRIE.add(tokenize_split(residence))
