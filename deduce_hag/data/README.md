# Data files

## Filter files
You can filter one file from another using grep, e.g.:
```
grep -xvf generieke_namen.lst achternaam.lst > achternaam-fltrd.lst

mv achternaam-fltrd.lst achternaam.lst
```